﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;


namespace FlashCardsGame.Model
{
    public class FlashCards
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string PlWord { get; set; }
        public string EnWord { get; set; }
        public virtual int LvlLearnId
        {
            get
            {
                return (int)this.LvlLearned;
            }
            set
            {
                LvlLearned = (KnowlageLvl)value;
            }
        }
        [NotNull]
        public KnowlageLvl LvlLearned { get; set; }
    }
    public enum KnowlageLvl
    {
        DontShow = -1,
        NotShown = 0,
        FirstShown = 1,
        SecondShown = 2,
        ThirdShown = 3,
        LastShown = 4,
        Learned = 5
    }
}
