﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashCardsGame.Model
{
    public class GameSettings
    {
        [PrimaryKey, AutoIncrement]
        public int Id { set; get; }
        public string SettingName { get; set; }
        public string Value { get; set; }

    }
}
