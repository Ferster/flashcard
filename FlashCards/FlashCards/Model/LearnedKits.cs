﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashCardsGame.Model
{
    public class LearnedKits
    {
        [PrimaryKey,AutoIncrement]
        public int Id { get; set; }
        public int KitNumber { get; set; }//numer zestawu
        public int KitLength { get; set; }//jego długosc
        public DateTime Date { get; set; }
    }
}
