﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FlashCardsGame.Controls
{
    public sealed partial class CustomButtonControl : UserControl
    {
        public CustomButtonControl()
        {
            this.InitializeComponent();
        }
        //private void ButtonClicked(object sender, RoutedEventArgs e)
        //{
        //    //OutputText.Text = string.Format("Hello {0}", NameInput.Text);
        //    //myContent = "aa";
        //}
        public String Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Label.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(CustomButtonControl), new PropertyMetadata(null));

        public bool IsEnabled
        {
            get;set;
        }


        public void ChangeVisiblity()
        {
            //get { return (string)GetValue(TextProperty); }
            //set { SetValue(TextProperty, value); }
            if (BorderFront.Visibility == Visibility.Collapsed)
                BorderFront.Visibility = Visibility.Visible;

        }

        public SolidColorBrush BorderColor
        {
            get { return (SolidColorBrush)GetValue(BorderProperty); }
            set { SetValue(BorderProperty, value); }
        }
        public static readonly DependencyProperty BorderProperty =
            DependencyProperty.Register("BorderColor", typeof(string), typeof(CustomButtonControl), new PropertyMetadata(null));



        public ImageSource ImageSource
        {
            get { return GetValue(ImageSourceProperty) as ImageSource; }
            set { SetValue(ImageSourceProperty, value); }
        }
        public static readonly DependencyProperty ImageSourceProperty =
           DependencyProperty.Register("ImageSource", typeof(string), typeof(CustomButtonControl), new PropertyMetadata(null));



        private void Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if(IsEnabled)
            {
                BorderFront.Visibility = Visibility.Collapsed;
            }
            //BorderFront.Background = new SolidColorBrush(Color.FromArgb(0, 255, 0, 0));
           
        }
    }
}
