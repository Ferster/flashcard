﻿using FlashCardsGame.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashCardsGame.ViewModel
{
    public class FlashCardsViewModel
    {
        SQLiteConnection dbConn;
        Dictionary<string, string> gameSettings = new Dictionary<string, string>();
        GameSettingsViewModel gameSettingsVM = new GameSettingsViewModel();
        LearnedKitsViewModel learnedKitsVM = new LearnedKitsViewModel();
        ObservableCollection<FlashCards> flashCardKit = new ObservableCollection<FlashCards>();
        Random random = new Random();
        private static int _currentKit;


        public FlashCards GetFlashCard(int flashCardId)
        {
            using (dbConn = new SQLiteConnection(App.DB_PATH))
            {
                var fc = dbConn.Query<FlashCards>("select * from FlashCards where Id =" + flashCardId).FirstOrDefault();
                return fc;
            }
        }
        public ObservableCollection<FlashCards> GetAllFlashCards()
        {
            using (var dbConn = new SQLiteConnection(App.DB_PATH))
            {
                List<FlashCards> myCollection = dbConn.Table<FlashCards>().ToList<FlashCards>();
                ObservableCollection<FlashCards> FlashCardsList = new ObservableCollection<FlashCards>(myCollection);
                return FlashCardsList;
            }
        }

        public void UpdateFlashCard(FlashCards fc)
        {
            using (var dbConn = new SQLiteConnection(App.DB_PATH))
            {
                var existingFlashCard = GetFlashCard(fc.Id);
                if (existingFlashCard != null)
                {
                    existingFlashCard.PlWord = fc.PlWord;
                    existingFlashCard.EnWord = fc.EnWord;
                    existingFlashCard.LvlLearned = (KnowlageLvl)fc.LvlLearnId;

                    dbConn.Update(existingFlashCard);
                    
                }
            }
        }

        public void InsertFlashCard(FlashCards newfc)
        {
            using (var dbConn = new SQLiteConnection(App.DB_PATH))
            {
                dbConn.RunInTransaction(() =>
                {
                    dbConn.Insert(newfc);
                });
            }
        }
        public void InsertFlashCard(List<FlashCards> newfcs)
        {
            using (var dbConn = new SQLiteConnection(App.DB_PATH))
            {
                dbConn.RunInTransaction(() =>
                {
                    foreach (var fc in newfcs)
                    {
                        dbConn.Insert(fc);
                    }
                });
            }
        }

        public void DeleteAllFlashCards()
        {
            using (var dbConn = new SQLiteConnection(App.DB_PATH))
            {
                dbConn.DropTable<FlashCards>();
                dbConn.CreateTable<FlashCards>();
                dbConn.Dispose();
                dbConn.Close();
            }
        }

        public ObservableCollection<FlashCards> GenerateKit()
        {
            return GenerateKit(-1);//losuj
        }
        public ObservableCollection<FlashCards> GenerateKit(int kitNumber)
        {
            gameSettings = gameSettingsVM.GetSettings();
            var allFlashCards = GetAllFlashCards();
            int _stepSize = (int)Math.Ceiling((double)allFlashCards.Count / Int32.Parse(gameSettings["KitSize"]));//round up
            if(kitNumber<0)
                 kitNumber = random.Next(_stepSize);
            _currentKit = kitNumber;
            for (int i = kitNumber; i < allFlashCards.Count - 1; i += _stepSize)
            {
                flashCardKit.Add(allFlashCards.ElementAt(i));
            }
            return flashCardKit;
        }
        public List<FlashCards> GetLearnedKit(int kitNumber, int kitLength)
        {
            List<FlashCards> tmp = new List<FlashCards>();
            var allFlashCards = GetAllFlashCards();
            int _stepSize = (int)Math.Ceiling((double)allFlashCards.Count / kitLength);//round up
            for (int i = kitNumber; i < allFlashCards.Count - 1; i += _stepSize)
            {
                tmp.Add(allFlashCards.ElementAt(i));
            }
            //dorobić sprawdzenie czy ilość slów jest wystarczająca, jeśli nie dobrać z poprzedniego zestawu
            return tmp;
        }
        public void SelectedAnswer(FlashCards fc, Answer a)
        {
            if (a == Answer.Yes)
            {
                fc.LvlLearned++;
                UpdateFlashCard(fc);
            }
            else if (a == Answer.No)
            {
                // ??
            }
            else if (a == Answer.DontShowNeverAgain)
            {
                fc.LvlLearned = KnowlageLvl.DontShow;
                UpdateFlashCard(fc);
            }

        }

        internal void SaveLearnedKit()
        {
            learnedKitsVM.InsertLearnedKit(new LearnedKits() { Date=DateTime.Now, KitLength = Int32.Parse(gameSettings["KitSize"]), KitNumber=_currentKit } );
        }
    }
    public enum Answer
    {
        DontShowNeverAgain = -1,
        No = 0,
        Yes = 1
    }
}
