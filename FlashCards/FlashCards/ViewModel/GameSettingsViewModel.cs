﻿using FlashCardsGame.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashCardsGame.ViewModel
{
    public class GameSettingsViewModel
    {
       
        SQLiteConnection dbConn;

        public void InitialSettings()
        {
            List<GameSettings> gs = new List<GameSettings>();                  
            gs.Add(new GameSettings() { SettingName = "KitSize",Value = 40.ToString() });
            gs.Add(new GameSettings() { SettingName = "Sound", Value = 1.ToString() });
            CreateInitialSettings(gs);
        }
        public Dictionary<string,string> GetSettings()
        {
            using (dbConn = new SQLiteConnection(App.DB_PATH))
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                var all = dbConn.Query<GameSettings>("select * from GameSettings");
                foreach (var item in all)
                    dic.Add(item.SettingName, item.Value);
                return dic;
            }
        }

        public GameSettings GetSetting(string settingName)
        {
            using (dbConn = new SQLiteConnection(App.DB_PATH))
            {
                 return dbConn.Query<GameSettings>("select * from GameSettings where SettingName = '"+settingName+"'").FirstOrDefault();
            }
        }
        public void UpdateSetting(GameSettings gs)
        {
            using (var dbConn = new SQLiteConnection(App.DB_PATH))
            {
                var currSetting = GetSetting(gs.SettingName);
                if(currSetting!=null)
                {
                    currSetting.Value = gs.Value;
                    dbConn.Update(currSetting);
                }
            }
        }

        //public void UpdateFlashCard(FlashCards fc)
        //{
        //    using (var dbConn = new SQLiteConnection(App.DB_PATH))
        //    {
        //        var existingFlashCard = GetFlashCard(fc.Id);
        //        if (existingFlashCard != null)
        //        {
        //            existingFlashCard.PlWord = fc.PlWord;
        //            existingFlashCard.EnWord = fc.EnWord;
        //            existingFlashCard.LvlLearned = (KnowlageLvl)fc.LvlLearnId;

        //            dbConn.Update(existingFlashCard);

        //        }
        //    }
        //}

        //public void UpdateSettings(GameSettings setting)
        //{
        //    using (var dbConn = new SQLiteConnection(App.DB_PATH))
        //    {
        //        var _stgs = dbConn.Query<GameSettings>("select * from GameSettings where SettingGame = '" + setting.SettingName + "'");
        //        if (_stgs != null)
        //        {
        //            dbConn.Update(setting);
        //        }
        //    }
        //}

        public void CreateInitialSettings(List<GameSettings> setting)
        {
            using (var dbConn = new SQLiteConnection(App.DB_PATH))
            {
                dbConn.RunInTransaction(() =>
                {
                    foreach(var stg in setting)
                        dbConn.Insert(stg);
                });
            }
        }

        public void DeleteSettings(GameSettings setting)
        {
            using (var dbConn = new SQLiteConnection(App.DB_PATH))
            {
                var existingSettings = dbConn.Query<GameSettings>("select * from GameSettings where SettingGame = '"+setting.SettingName+"'").FirstOrDefault();
                if (existingSettings != null)
                    dbConn.Delete(existingSettings);
            }
        }
    }
}
