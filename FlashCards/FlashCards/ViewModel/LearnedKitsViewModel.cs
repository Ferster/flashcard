﻿using FlashCardsGame.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashCardsGame.ViewModel
{
    public class LearnedKitsViewModel
    {
        SQLiteConnection dbConn;

        public void InitialSettings()
        {
            LearnedKits kit = new LearnedKits() {Date = DateTime.Now, KitLength = 40, KitNumber=2 };
            InsertLearnedKit(kit);
        }

        public List<LearnedKits> GetLastLearnedKit(int limit)
        {
            using (dbConn = new SQLiteConnection(App.DB_PATH))
            {
                return dbConn.Query<LearnedKits>("select * from LearnedKits Order By Id Desc LIMIT "+limit);
            }
        }
        public List<LearnedKits> GetAllLearnedKits()
        {
            using (dbConn = new SQLiteConnection(App.DB_PATH))
            {
                return dbConn.Query<LearnedKits>("select * from LearnedKits Order By Id Desc").ToList();
            }
        }
        public void InsertLearnedKit(LearnedKits kit)
        {
            using (var dbConn = new SQLiteConnection(App.DB_PATH))
            {
                dbConn.RunInTransaction(() =>
                {
                    dbConn.Insert(kit);
                });
            }
        }
    }
}
