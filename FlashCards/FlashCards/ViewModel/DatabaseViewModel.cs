﻿using SQLite;
using FlashCardsGame.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlashCardsGame;
using FlashCardsGame.Sources;
using System.IO;
using Windows.Storage;


namespace FlashCardsGame.ViewModel
{
    public class DatabaseViewModel
    {
        SQLiteConnection dbConn;
       
        //Create Tabble 
        public async Task<bool> onCreate()
        {
            try
            {
                
                if (!CheckFileExists(App.DB_NAME).Result)
                {
                    using (dbConn = new SQLiteConnection(App.DB_PATH))
                    {
                         dbConn.CreateTable<FlashCards>();
                         dbConn.CreateTable<GameSettings>();
                         dbConn.CreateTable<LearnedKits>();
                    }
                    new FreeDictionary2kWords();
                    new GameSettingsViewModel().InitialSettings();
#if DEBUG
                    new LearnedKitsViewModel().InitialSettings();//do wywalenia - tylko na potrzeby testów
#endif

                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        private async Task<bool> CheckFileExists(string fileName)
        {
            try
            {
                var store = await Windows.Storage.ApplicationData.Current.LocalFolder.GetFileAsync(fileName);
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
