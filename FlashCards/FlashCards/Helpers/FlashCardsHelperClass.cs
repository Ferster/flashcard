﻿using FlashCardsGame.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashCardsGame.Helpers
{
    class FlashCardsHelperClass
    {
        DatabaseHelperClass Db_Helper = new DatabaseHelperClass();
        GameSettings gameSettings = new GameSettings();
        ObservableCollection<FlashCard> flashCardKit = new ObservableCollection<FlashCard>();
        Random random = new Random();
        public FlashCardsHelperClass(GameSettings gs)
        {
            gameSettings = gs;
        }
        public ObservableCollection<FlashCard> GetAllFlashCards()
        {
            return Db_Helper.GetAllFlashCards();
        }
        public ObservableCollection<FlashCard> GenerateKit()
        {
            return GenerateKit(-1);//losuj
        }
        public ObservableCollection<FlashCard> GenerateKit(int kitNumber)
        {            
            var allFlashCards = GetAllFlashCards();
            int _stepSize = (int)Math.Ceiling((double)allFlashCards.Count / gameSettings.KitSize);//round up
            if(kitNumber<0)
                 kitNumber = random.Next(_stepSize);

            for (int i = kitNumber; i < allFlashCards.Count - 1; i += _stepSize)
            {
                flashCardKit.Add(allFlashCards.ElementAt(i));
            }
            return flashCardKit;
        }
    }
}
