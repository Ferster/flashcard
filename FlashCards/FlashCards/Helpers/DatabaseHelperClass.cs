﻿using SQLite;
using FlashCardsGame.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlashCardsGame;


namespace FlashCardsGame.Helpers
{
    public class DatabaseHelperClass
    {
        SQLiteConnection dbConn;

        //Create Tabble 
        public async Task<bool> onCreate(string DB_PATH)
        {
            try
            {
                if (!CheckFileExists(DB_PATH).Result)
                {
                    using (dbConn = new SQLiteConnection(DB_PATH))
                    {
                        dbConn.CreateTable<FlashCard>();
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        private async Task<bool> CheckFileExists(string fileName)
        {
            try
            {
                var store = await Windows.Storage.ApplicationData.Current.LocalFolder.GetFileAsync(fileName);
                return true;
            }
            catch
            {
                return false;
            }
        }

        // Retrieve the specific contact from the database. 
        public FlashCard GetFlashCard(int flashCardId)
        {
            using (dbConn = new SQLiteConnection(App.DB_PATH))
            {
                var existingconact = dbConn.Query<FlashCard>("select * from FlashCards where Id =" + flashCardId).FirstOrDefault();
                return existingconact;
            }
        }
        // Retrieve the all contact list from the database. 
        public ObservableCollection<FlashCard> GetAllFlashCards()
        {
            using (var dbConn = new SQLiteConnection(App.DB_PATH))
            {
                List<FlashCard> myCollection = dbConn.Table<FlashCard>().ToList<FlashCard>();
                ObservableCollection<FlashCard> FlashCardsList = new ObservableCollection<FlashCard>(myCollection);
                return FlashCardsList;
            }
        }

        //Update existing conatct 
        public void UpdateFlashCard(FlashCard fc)
        {
            using (var dbConn = new SQLiteConnection(App.DB_PATH))
            {
                var existingFlashCard= dbConn.Query<FlashCard>("select * from FlashCards where Id =" + fc.Id).FirstOrDefault();
                if (existingFlashCard != null)
                {
                    existingFlashCard.PlWord = fc.PlWord;
                    existingFlashCard.EnWord = fc.EnWord;
                    existingFlashCard.LvlLearned = (KnowlageLvl)fc.LvlLearnId;
                    
                    dbConn.RunInTransaction(() =>
                    {
                        dbConn.Update(existingFlashCard);
                    });
                }
            }
        }
        // Insert the new contact in the FlashCards table. 
        public void Insert(FlashCard newfc)
        {
            using (var dbConn = new SQLiteConnection(App.DB_PATH))
            {
                dbConn.RunInTransaction(() =>
                {
                    dbConn.Insert(newfc);
                });
            }
        }
        public void Insert(List<FlashCard> newfcs)
        {
            using (var dbConn = new SQLiteConnection(App.DB_PATH))
            {
                dbConn.RunInTransaction(() =>
                {
                    foreach(var fc in newfcs)
                    {
                        dbConn.Insert(fc);
                    }
                });
            }
        }


        //Delete specific contact 
        public void DeleteFlashCard(int Id)
        {
            using (var dbConn = new SQLiteConnection(App.DB_PATH))
            {
                var existingFlashCard = dbConn.Query<FlashCard>("select * from FlashCards where Id =" + Id).FirstOrDefault();
                if (existingFlashCard != null)
                {
                    dbConn.RunInTransaction(() =>
                    {
                        dbConn.Delete(existingFlashCard);
                    });
                }
            }
        }
        //Delete all contactlist or delete FlashCards table 
        public void DeleteAllContact()
        {
            using (var dbConn = new SQLiteConnection(App.DB_PATH))
            {
                //dbConn.RunInTransaction(() => 
                //   { 
                dbConn.DropTable<FlashCard>();
                dbConn.CreateTable<FlashCard>();
                dbConn.Dispose();
                dbConn.Close();
                //}); 
            }
        }
    }
}
