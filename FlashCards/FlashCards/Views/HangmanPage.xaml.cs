﻿using FlashCardsGame.Model;
using FlashCardsGame.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace FlashCardsGame.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class HangmanPage : Page
    {
        FlashCardsViewModel flashCardVM = new FlashCardsViewModel();
        LearnedKitsViewModel learnedKitsVM = new LearnedKitsViewModel();
        List<FlashCards> flashCardList = new List<FlashCards>();
        private FlashCards fc = new FlashCards();
        private static int kitCounter=0;
        List<LearnedKits> allLearnedKits = new List<LearnedKits>();
        public HangmanPage()
        {
            allLearnedKits = learnedKitsVM.GetAllLearnedKits();
            this.InitializeComponent();
            readNextKit();
            takeWordFromList();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }


        private async void Letter_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ((StackPanel)sender).Visibility = Visibility.Collapsed;
            checkLetterIsCorrect(((StackPanel)sender).Tag.ToString());
            if (tbAnswer.Text.IndexOf("_") == -1)
            {
                await System.Threading.Tasks.Task.Delay(TimeSpan.FromMilliseconds(300));
                takeWordFromList();
                redrawKeyboard();
            }
        }
        private async void readNextKit()
        {
            if (allLearnedKits.Count > kitCounter)
            {
                flashCardList = flashCardVM.GetLearnedKit(allLearnedKits[kitCounter].KitNumber, allLearnedKits[kitCounter].KitLength);
                kitCounter++;
            }
            else
            {
                var dialog = new MessageDialog("brak zestawów do powtórzenia, przejdź do modułu nauki");
                await dialog.ShowAsync();
            }
            
        }
        private async void checkLetterIsCorrect(string letter)
        {

            if (fc.EnWord.IndexOf(letter.ToLower()) != -1)
            {
                int i = 0;
                StringBuilder sb = new StringBuilder(tbAnswer.Text);

                while ((i = fc.EnWord.IndexOf(letter.ToLower(), i)) != -1)
                {
                    sb.Replace("_", letter, (((i + 1) * 2) - 2), 1);
                    i++;
                    tbPoints.Text = (Int32.Parse(tbPoints.Text) + 1).ToString();
                }
                tbAnswer.Text = sb.ToString().ToUpper();
            }
            else
            {
                if (IsGameOver())
                {
                    await System.Threading.Tasks.Task.Delay(TimeSpan.FromMilliseconds(600));
                    var dialog = new MessageDialog("Koniec gry");
                    //dialog.Title = "Koniec gry";
                    dialog.Commands.Add(new UICommand("Zamknij", new UICommandInvokedHandler(commandHandlers)));
                    dialog.Commands.Add(new UICommand("Zagraj ponownie", new UICommandInvokedHandler(commandHandlers)));
                    
                    await dialog.ShowAsync();
                    hideHangman();//to trzeba będzie przenieść jak obsłuże messagebox
                    tbPoints.Text = "0";       
                }
            }
        }

        private FlashCards takeWordFromList()
        {
            if(flashCardList.Count!=0)
            {
                fc = flashCardList[new Random().Next(flashCardList.Count())];
                tbWord.Text = fc.PlWord;
                flashCardList.Remove(fc);
                tbAnswer.Text = initialUndercourse(fc.EnWord);
            }
            else
            {
                readNextKit();
            }
            
            return fc;
        }
        private string initialUndercourse(string word)
        {
            string _undercourse="";
            for (int i = 0; i < word.Length; i++)
                _undercourse += "_ ";
            return _undercourse;
        }
        private void redrawKeyboard()
        {
            foreach(var keyboard in HangmanKeyboard.Children)
            {
                (keyboard as StackPanel).Visibility = Visibility.Visible;
            }
        }
        private void hideHangman()
        {
            foreach(var hangman in HangmanChalk.Children)
            {
                (hangman as Image).Visibility = Visibility.Collapsed;
            }

        }
        private bool IsGameOver()
        {
            foreach (var changman in HangmanChalk.Children)
            {
                if ((changman as Image).Visibility == Visibility.Collapsed)
                {
                    (changman as Image).Visibility = Visibility.Visible;
                    if ((changman as Image).Tag != null)
                        return true;
                    else
                        return false;
                }   
            }
            return false;
        }
        private void commandHandlers(IUICommand commandLabel)
        {
            switch (commandLabel.Label.ToString())
            {
                //Okay Button.
                case "Zamknij":
                    //Frame.Navigate(typeof(GamesPage));
                    Frame.GoBack();
                    break;
                //Quit Button.
                case "Zagraj ponownie":
                    Frame.Navigate(typeof(HangmanPage));
                    break;

            }
        }
    }
}
