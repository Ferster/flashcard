﻿using FlashCardsGame.Model;
using FlashCardsGame.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace FlashCardsGame.Views
{
    public sealed partial class SettingsPage : Page
    {
        private GameSettingsViewModel gameSettingsVM = new GameSettingsViewModel();
        private Dictionary<string, string> gameSettings = new Dictionary<string, string>();
        public SettingsPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            gameSettings = gameSettingsVM.GetSettings();
            sliderKitLength.Value = Int32.Parse(gameSettings["KitSize"]); 
        }

        private void sliderKitLength_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            try
            {
                tbKitLength.Text = sliderKitLength.Value.ToString();
            }
            catch(Exception){}
                
        }


        private void sliderKitLength_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            gameSettingsVM.UpdateSetting(new GameSettings() { SettingName = "KitSize", Value = sliderKitLength.Value.ToString() });
        }
    }
}
