﻿using FlashCardsGame.Model;
using FlashCardsGame.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;
using System.Threading.Tasks;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace FlashCardsGame.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LearnPage : Page
    {
        ObservableCollection<FlashCards> flashCardKit = new ObservableCollection<FlashCards>();
        GameSettingsViewModel gameSettingsViewModel = new GameSettingsViewModel();
        FlashCardsViewModel flashCardViewModel = new FlashCardsViewModel();
        private FlashCards _currentFC = new FlashCards();
        private Dictionary<string,string> _gameSettings = new Dictionary<string,string>();
        private Random _random = new Random();

        public LearnPage()
        {
            this.InitializeComponent();
        }
       
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            flashCardKit = flashCardViewModel.GenerateKit();
            _gameSettings = gameSettingsViewModel.GetSettings();
            tblockWord.Text = getNextFlashCard().EnWord;
            pbProgress.Maximum = Int32.Parse(_gameSettings["KitSize"]);
           // tbDebug.Text = lkvm.GetLastXLearnedKit(1).FirstOrDefault().Date+"";
        }
# region Buttons
         private void btnYes_Click(object sender, RoutedEventArgs e)
        {
            
            if (flashCardKit.Count > 0)
            {
                tblockWord.Text = getNextFlashCard().EnWord;
                changeButtonsState();
                flashCardViewModel.SelectedAnswer(_currentFC, Answer.Yes);
                pbProgress.Value += 1;
            }
            else
            {

                flashCardViewModel.SaveLearnedKit();
                showMessageDialog("Koniec");
            }
                
        }

        private void btnNo_Click(object sender, RoutedEventArgs e)
        {
            flashCardKit.Add(_currentFC);
            changeButtonsState();
            tblockWord.Text = getNextFlashCard().EnWord;
        }
        private void btnShow_Click(object sender, RoutedEventArgs e)
        {
            tblockWord.Text = _currentFC.PlWord;
            changeButtonsState();
        }

        private void btnTrash_Click(object sender, RoutedEventArgs e)
        {
            tblockWord.Text = getNextFlashCard().EnWord;
            flashCardViewModel.SelectedAnswer(_currentFC, Answer.DontShowNeverAgain);
            pbProgress.Value += 1;
            if(btnShow.Visibility == Visibility.Collapsed)
                changeButtonsState();
        }
#endregion

#region private methods
        
        private FlashCards getNextFlashCard()
        {

            _currentFC=flashCardKit[_random.Next(flashCardKit.Count())];
            flashCardKit.Remove(_currentFC);
            return _currentFC;
        }
        private void showMessageDialog(string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = "Koniec zestawu. Możesz wylosować następny lub przejść do gier";
            dialog.Commands.Add(new UICommand("Losuj", new UICommandInvokedHandler(commandHandlers)));
            dialog.Commands.Add(new UICommand("Gry", new UICommandInvokedHandler(commandHandlers)));
            var res = dialog.ShowAsync();
        }
        private void commandHandlers(IUICommand commandLabel)
        {
            switch (commandLabel.Label.ToString())
            {
                //Okay Button.
                case "Losuj":
                   // StartPage.Focus(Windows.UI.Xaml.FocusState.Pointer);
                    break;
                //Quit Button.
                case "Gry":
                    Frame.Navigate(typeof(GamesPage));
                    break;
                //end.
            }
        }
        private void changeButtonsState()
        {
            if (btnShow.Visibility == Visibility.Collapsed)
            {
                btnNo.Visibility = Visibility.Collapsed;
                btnShow.Visibility = Visibility.Visible;
                btnYes.Visibility = Visibility.Collapsed;
            }
            else
            {
                btnNo.Visibility = Visibility.Visible;
                btnShow.Visibility = Visibility.Collapsed;
                btnYes.Visibility = Visibility.Visible;
            }
        }
#endregion

    }
  
}
