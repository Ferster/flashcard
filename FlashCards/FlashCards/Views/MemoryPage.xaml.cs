﻿using FlashCardsGame.Controls;
using FlashCardsGame.Model;
using FlashCardsGame.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace FlashCardsGame.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MemoryPage : Page
    {
        FlashCardsViewModel flashCardVM = new FlashCardsViewModel();
        LearnedKitsViewModel learnedKitsVM = new LearnedKitsViewModel();
        List<FlashCards> flashCardList = new List<FlashCards>();
        private FlashCards fc = new FlashCards();
        private static int kitCounter = 0;
        List<LearnedKits> allLearnedKits = new List<LearnedKits>();
        Random r = new Random();

        public MemoryPage()
        {
            this.InitializeComponent();
            allLearnedKits = learnedKitsVM.GetAllLearnedKits();
            readNextKit();
            generateGrid(7, 2);
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
   
        }
        public void generateGrid(int row, int column)
        {
            int x = 0;
            for (int i = 0;i< row;i++)
                myGrid.RowDefinitions.Add(new RowDefinition());

            for (int i = 0; i < column; i++)
                myGrid.ColumnDefinitions.Add(new ColumnDefinition());

            
            for (int i = 0; i < column; i++)
            {
                
                for (int j = 0; j < row; j++)
                {
                    CustomButtonControl b = new CustomButtonControl() {};// { Height = 50, Width = 150 };
                    b.SetValue(Grid.ColumnProperty, i);
                    b.SetValue(Grid.RowProperty, j);
                    b.IsEnabled = true;
                    x++;
                    //b.Tag = x.ToString();
                    b.BorderColor = new SolidColorBrush(Color.FromArgb(255, 0, 0, 255));
                    myGrid.Children.Add(b as CustomButtonControl);
                    b.Tapped += new TappedEventHandler(Button_Click);
                    b.ImageSource = new BitmapImage(new Uri("ms-appx:///Images/Memory/jeans.jpg"));
                }
            }
            List<WordAndId> _words = new List<WordAndId>();
            while (_words.Count < row * column)
            {
                var card = getNextWord();
                _words.Add(new WordAndId() { word = card.EnWord, id = _words.Count });
                _words.Add(new WordAndId() { word = card.PlWord, id = _words.Count-1 });
            }
            var shuffledcards = _words.OrderBy(a => Guid.NewGuid()).ToList();

            foreach (CustomButtonControl b in myGrid.Children)
            {
                var item = shuffledcards.FirstOrDefault();
                shuffledcards.Remove(item);
                b.Text = item.word;
                b.Tag = item.id;
            }
        }

        CustomButtonControl clickedButton;
        private async void Button_Click(object sender, TappedRoutedEventArgs e)
        {
            if (clickedButton == null)
            {
                clickedButton = new CustomButtonControl();
                clickedButton = (CustomButtonControl)sender;
            }
                
            else
            {
                if(clickedButton.Tag.ToString() == ((CustomButtonControl)sender).Tag.ToString())
                {
                   
                    clickedButton = null;
                    //zmienic kolor na np. zielony
                }
                else
                {
                    clickedButton.IsEnabled = false;
                    ((CustomButtonControl)sender).IsEnabled = false;
                    await System.Threading.Tasks.Task.Delay(TimeSpan.FromMilliseconds(400));
                    clickedButton.ChangeVisiblity();
                    ((CustomButtonControl)sender).ChangeVisiblity();
                    ((CustomButtonControl)sender).IsEnabled=true;
                    clickedButton.IsEnabled = true;
                    clickedButton = null;
                   

                }
            }
        }
        private  void readNextKit()
        {
            if (allLearnedKits.Count > kitCounter)
            {
                flashCardList = flashCardVM.GetLearnedKit(allLearnedKits[kitCounter].KitNumber, allLearnedKits[kitCounter].KitLength);
                kitCounter++;
            }
        }
        private FlashCards getNextWord()
        {
            if(flashCardList.Count>0)
            {
                var card = flashCardList.ElementAt(r.Next(flashCardList.Count - 1));
                flashCardList.Remove(card);
                return card;
            }
            else
            {
                readNextKit();
            }
            return null;
        } 
        struct WordAndId
        {
            public string word
            {
                get; set;
            }
            public int id
            {
                get; set;
            }
        }
    }
}
